/**
 * Include all directives that should be accessible globally
 */
import { PLATFORM_DIRECTIVES, provide } from '@angular/core';
import { ROUTER_DIRECTIVES } from "@angular/router";
import { MaterializeDirective } from "angular2-materialize";


export const DEFAULT_DIRECTIVES: any = [
  ROUTER_DIRECTIVES,
  MaterializeDirective,
];

export const GLOBAL_PROVIDERS: any = DEFAULT_DIRECTIVES.map(dir =>
  provide(PLATFORM_DIRECTIVES, {useValue: dir, multi: true})
);
