#!/usr/bin/env bash

pm2 start http-server --name co-jemy-front -- ./dist -p 8080 -d false
pm2 logs